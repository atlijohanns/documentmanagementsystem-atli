﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentManagementSystem
{
    public partial class ArchiveUser : Form
    {
        public ArchiveUser()
        {
            InitializeComponent();
        }

        private void btnSubmitArchiveUser_Click(object sender, EventArgs e)
        {
            if (sqlWorkBench.userExists(txtArchiveUsername.Text))
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to archive user?", "Sure", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    sqlWorkBench.updateUserDetails(5, "1", txtArchiveUsername.Text);

                    MessageBox.Show("User archived.");

                    txtArchiveUsername.Text = string.Empty;
                }
            }
            else
            {
                MessageBox.Show("User does not exist.");

                txtArchiveUsername.Text = string.Empty;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;

namespace DocumentManagementSystem
{

    public partial class CreateDocument : Form
    {
        public CreateDocument(string username)
        {
            InitializeComponent();

            //string Username = username;
            lblCreateDocumentRecord.Text = username;

        }

        public void btnBrowse_Click(object sender, EventArgs e)
        {

            // frmLoginScreen UserDetails = frmLoginScreen();
            string Username = lblCreateDocumentRecord.Text;


            DialogResult result = openFileDialog1.ShowDialog(); // result will be TRUE or FALSE depending on whether or not the file browser opened correctly or not


            string AuthorFolder = @"C:/Programs/Local_Server/" + Username + "/";
            //MessageBox.Show(AuthorFolder);


            if (!File.Exists(@"C:/Programs/Local_Server/"))
            {

                Directory.CreateDirectory(AuthorFolder); // use the user name to create a seperate folder if there isnt one already

            }
            //else //create folder
            //{
            //     AuthorFolder
            //}

            // the upload code
            if (result == DialogResult.OK)// if the file browser opened correctly
            {
                string FilePath = openFileDialog1.FileName; // contains the URL, therefore FilePath will be put into the document record database
                                                            //MessageBox.Show(FilePath);
                                                            //string Destination = AuthorFolder + 
                lblSourceURL.Text = FilePath;
                string CompletePath = AuthorFolder + Path.GetFileName(FilePath);
                lblTargetURL.Text = CompletePath;
                //using (FileStream DestPath = new FileStream(FilePath, FileMode.Open)
                //File.Copy(FilePath, (AuthorFolder + Path.GetFileName(FilePath)), true); // copies the file to the AuthorFolder/filename.extension


            }

            else
            {
                MessageBox.Show("File Explorer failed to open.");
                return;
            }
        }

        private void btnCancelCreateNewDocument_Click(object sender, EventArgs e)
        {
            this.Hide(); // to go back to the Author Form

        }

        private void btnSubmitNewDocument_Click(object sender, EventArgs e)
        {
            // setting the data that will be entered into the database
            string documentTitle = txtDocumentTitle.Text;
            int RevNum = (int)txtRevNum.Value;

            string Author = lblCreateDocumentRecord.Text;
            // int Status = 1;
            int Status = 0;
            string actDate = "NULL";
            string Description = txtDescription.Text;

            //Test xyz = new Test();
            string SourceURL = lblSourceURL.Text;
            string TargetURL = lblTargetURL.Text;

            // validation checks
            if (documentTitle == "" | documentTitle == " ")
            {
                MessageBox.Show("Please enter a Document Title before submitting");
                return;
            }

            if (RevNum == 0)
            {
                MessageBox.Show("Please enter a revision number.");
                return;
            }
            // checking what if any checkbox is checked
            if (ckbxActive.Checked)
            {
                Status = 0;
                actDate = DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString();
                // MessageBox.Show(Status);
            }
            else if (ckbxDraft.Checked)
            {
                Status = 1;
                actDate = "NULL";
            }
            else
            {
                MessageBox.Show("Please check either Active or Draft Status.");
                return;
            }

            if (Description == "" | Description == " ")
            {
                MessageBox.Show("Please enter a Description.");
                return;
            }


            // //if(documentTitle == ""| RevNum)

            SQLiteConnection sqlite;
            string dbURL = Directory.GetCurrentDirectory() + "/../../db/IP3database.db"; // link to the database

            sqlite = new SQLiteConnection("Data Source=" + dbURL); // establishes connection to the database
            sqlite.Open(); // opens the connection

            // prepares the query that will enter the variables from the CreateDoc form to the database Documents
            string query = "insert into Documents(title, revNum, author, status, desc,url,actDate,createDate) values ('" + documentTitle + "','" + RevNum + "','" + Author + "','" + Status + "','" + Description + "','" + TargetURL + "','" + actDate + "', 0);";


            // string x = "insert into Users(username, password, forename, surname, role, status) values ('" + username + "', '" + password + "', '" + forename + "', '" + surname + "', " + role + ", 0);";
            SQLiteCommand cmd = new SQLiteCommand(query, sqlite); // establishes the command to be run
            cmd.ExecuteNonQuery(); // executes the above command
            sqlite.Close(); // ends the SQLite connection

            File.Copy(SourceURL, (TargetURL), true); // copies the file to the AuthorFolder/filename.extension
            MessageBox.Show("The file has been copied to the target url");
            this.Hide();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtRevNum_KeyDown(object sender, KeyEventArgs e)
        {
            //if (!char.IsControl(e.KeyValue) && !char.IsDigit(e.KeyChar)) && (e.KeyChar != "."))
            //{
            //    e.Handled = true;
            //}
        }

        private void txtRevNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!char.IsControl(e.KeyChar)
            //    && !char.IsDigit(e.KeyChar)
            //    && (e.KeyChar != '.'))
            //{
            //    e.Handled = true;
            //}
        }

        private void lblFileURL_Click(object sender, EventArgs e)
        {

        }

        private void lblAttach_Click(object sender, EventArgs e)
        {

        }
    }
}
